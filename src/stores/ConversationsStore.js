import BaseStore from './BaseStore';
import { LOAD_CONVERSATIONS, LOAD_CONVERSATIONS_SUCCEED } from '../constants/ConversationConstants';
import ApiManager from '../managers/ApiManager';

class ConversationsStore extends BaseStore {
  constructor() {
    super();
    this.subscribe(() => this._subscribe.bind(this));
    this._conversation = null;
    this._conversations = null;
  }

  _subscribe(action) {
    switch(action.type) {
    case LOAD_CONVERSATIONS:
      console.log(LOAD_CONVERSATIONS);
      ApiManager.getConversations();
      break;
    case LOAD_CONVERSATIONS_SUCCEED:
      console.log(LOAD_CONVERSATIONS_SUCCEED, action);
      this._conversations = action.conversations;
      this._conversation = this.conversations[0];
      this.emitChange();
      break;
    default:
      break;
    }
  }

  get conversations() {
    return this._conversations;
  }

  get conversation() {
    return this._conversation;
  }
}

export default new ConversationsStore();
