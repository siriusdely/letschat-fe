import BaseStore from './BaseStore';
import { LOAD_CONTACTS, LOAD_CONTACTS_SUCCEED } from '../constants/ContactConstants';
import ApiManager from '../managers/ApiManager';

class ContactsStore extends BaseStore {
  constructor() {
    super();
    this.subscribe(() => this._subscribe.bind(this));
    this._contacts = null;
  }

  _subscribe(action) {
    switch(action.type) {
    case LOAD_CONTACTS:
      ApiManager.getContacts();
      break;
    case LOAD_CONTACTS_SUCCEED:
      this._contacts = action.contacts;
      this.emitChange();
      break;
    default:
      break;
    }
  }

  get contacts() {
    return this._contacts;
  }
}

export default new ContactsStore();
