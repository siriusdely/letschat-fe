let JSON = require('circular-json');

const LStorage = {
  setObject: (key, obj) => {
    localStorage.setItem(key, JSON.stringify(obj));
  },
  getObject: (key) => {
    const obj = localStorage.getItem(key);
    return obj && JSON.parse(obj);
  },
  removeObject: (key) => {
    localStorage.removeItem(key);
  }
}

module.exports = LStorage;
