import BaseStore from './BaseStore';
import {
  RECEIVE_TEXT, SEND_TEXT, MESSAGE_SENT, READ_MESSAGE
  , MESSAGE_DELIVERED, MESSAGE_READ
} from '../constants/MessageConstants';
import MessageType from '../constants/MessageType';
import MessageStatus from '../constants/MessageStatus';

import ConnectionManager from '../managers/ConnectionManager';
import ConversationsStore from './ConversationsStore';
import UnreadsStore from './UnreadsStore';

class MessagesStore extends BaseStore {
  constructor() {
    super();
    this.subscribe(() => this._subscribe.bind(this));
    this._unsents = {};
    this._sents = {};
    this._conversation = null;
    ConversationsStore.addChangeListener(() => {
      if (this.conversation !== ConversationsStore.topic) {
        this._conversation = ConversationsStore.topic;
        this.emitChange();
      }
    });
  }

  _subscribe(action) {
    switch(action.type) {

    case RECEIVE_TEXT: {
      console.log('RECEIVE_TEXT', action);
      const { conversationId, senderId, messageId, content, startedAt } = action;

      let sents = this._sents[conversationId] || [];
      const received = sents.find(s => s.id === messageId);
      if (received) { break; }

      sents = [].concat(sents, {
        id: messageId, type: MessageType.TEXT,
        conversationId, senderId, content,
        status: MessageStatus.RECEIVED, startedAt: startedAt || new Date()
      });
      this._sents[conversationId] = sents;
      this.emitChange();

      UnreadsStore.increment(conversationId);
      break;
    }
    case READ_MESSAGE: {
      console.log('READ_MESSAGE', action);
      const { conversationId, senderId, messageId } = action;

      let sents = this._sents[conversationId] || [];
      const sent = sents.find(s => s.id === messageId);
      if (!sent || sent.status === MessageStatus.RECEIVED_READ) { break; }

      sent.status = MessageStatus.RECEIVED_READ;
      ConnectionManager.sendRead(conversationId, messageId);
      this.emitChange();
      UnreadsStore.decrement(conversationId);
      break;
    }
    case SEND_TEXT: {
      console.log('SEND_TEXT', action);
      const { conversationId, content } = action;

      let unsents = this._unsents[conversationId] || [];
      let index = -1;
      const unsent = unsents.find((uns, idx) => {
        if (uns.conversationId === conversationId) {
          index = idx;
          return true;
        } else {
          return false;
        }
      });
      // console.log('unsent', unsent);
      if (unsent) {
        unsent.status = MessageStatus.FAILED;
        unsents = unsents.slice(0, index).concat(unsents.slice(index + 1));
        this._unsents[conversationId] = unsents;
      }

      ConnectionManager.sendText(conversationId, content);
      unsents = [ ...unsents,
                  { id: (unsents.length + 1), type: MessageType.TEXT, conversationId,
                    startedAt: new Date(), content,
                    status: MessageStatus.UNSENT } ];
      this._unsents[conversationId] = unsents;
      this.emitChange();
      break;
    }
    case MESSAGE_SENT: {
      console.log('MESSAGE_SENT', action);
      const { conversationId, messageId } = action;

      let unsents = this._unsents[conversationId] || [];
      let index = -1;
      const unsent = unsents.find((uns, idx) => {
        if (uns.conversationId === conversationId) {
          index = idx;
          return true;
        }
        return false;
      });
      if (!unsent) { break; }

      unsents = unsents.slice(0, index).concat(unsents.slice(index + 1));
      this._unsents[conversationId] = unsents;

      unsent.id = messageId;
      unsent.status = MessageStatus.SENT;
      let sents = this._sents[conversationId] || [];
      sents = [ ...sents, unsent ];
      this._sents[conversationId] = sents;
      this.emitChange();
      break;
    }
    case MESSAGE_DELIVERED: {
      console.log('MESSAGE_DELIVERED', action);
      const { conversationId, messageId } = action;

      let sents = this._sents[conversationId] || [];
      const sent = sents.find(s => s.id === messageId);
      if (!sent || sent.status === MessageStatus.READ) { break; }

      sent.status = MessageStatus.DELIVERED;
      this.emitChange();
      break;
    }
    case MESSAGE_READ: {
      console.log('MESSAGE_READ', action);
      const { conversationId, messageId } = action;

      let sents = this._sents[conversationId] || [];
      const sent = sents.find(s => s.id === messageId);
      if (!sent) { break; }

      sent.status = MessageStatus.READ;
      this.emitChange();
      break;
    }
    default:
      break;
    }
  }

  messages = (conversationId) => {
    // console.log('conversationId', conversationId);
    const sents = this._sents[conversationId] || [];
    const unsents = this._unsents[conversationId] || [];
    // console.log('sents', sents);
    // console.log('unsents', unsents);
    return sents.concat(unsents);
  }

  get conversation() {
    return this._conversation;
  }
}

export default new MessagesStore();
