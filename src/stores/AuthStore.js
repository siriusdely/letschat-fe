import BaseStore from './BaseStore';
import {
  ACCESS_TOKEN, USER,
  START_LOGIN, LOGIN_FAILED, LOGIN_SUCCEED
  , LOGOUT, UNAUTHORIZED
  , USERNAME, PASSWORD } from '../constants/AuthConstants';
import AuthManager from '../managers/AuthManager';
import ApiManager from '../managers/ApiManager';
import LStorage from './LStorage';

import ConnectionManager from '../managers/ConnectionManager';

class AuthStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(() => this._subscribe.bind(this));
    this._user = LStorage.getObject(USER);
    this._accessToken = LStorage.getObject(ACCESS_TOKEN);
    this._loggingIn = false;
    ApiManager.accessToken = this.accessToken;
    ApiManager.user = this.user;

    ConnectionManager.token = this.accessToken;
    ConnectionManager.user = this.user;
    if (this.user) ConnectionManager.connect();

    this._username = LStorage.getObject(USERNAME);
    this._password = LStorage.getObject(PASSWORD);
  }

  logout() {
    LStorage.removeObject(ACCESS_TOKEN);
    LStorage.removeObject(USER);

    this._accessToken = this._user = null;

    ApiManager.accessToken = ConnectionManager.token = ConnectionManager.user = null;
    ConnectionManager.disconnect();

    this._loggingIn = false;
    this.emitChange();
  }

  _subscribe(action) {
    switch(action.type) {
    case START_LOGIN:
      const { username, password } = action;
      this._username = username;
      this._password = password;
      LStorage.setObject(USERNAME, username);
      LStorage.setObject(PASSWORD, password);
      AuthManager.login(username, password);
      this._loggingIn = true;
      this.emitChange();
      break;
    case LOGIN_FAILED:
      // console.log(LOGIN_FAILED, action);
      const { error } = action;
      this._error = error;
      this._password = null;
      LStorage.removeObject(PASSWORD);
      this._loggingIn = false;
      this.emitChange();
      break;
    case LOGIN_SUCCEED:
      const { token, user } = action;

      LStorage.setObject(ACCESS_TOKEN, token);
      LStorage.setObject(USER, user);

      this._accessToken = token;
      this._user = user;

      ApiManager.accessToken = this.accessToken;

      ConnectionManager.token = this.accessToken;
      ConnectionManager.user = this.user;
      if (this.user) ConnectionManager.connect();

      this._loggingIn = false;
      this.emitChange();
      break;
    case LOGOUT:
      this.logout();
      break;
    case UNAUTHORIZED:
      this.logout();
      break;
    default:
      break;
    }
  }

  get accessToken() {
    return this._accessToken;
  }

  get user() {
    return this._user;
  }

  get error() {
    return this._error;
  }

  get username() {
    return this._username;
  }

  get password() {
    return this._password;
  }

  get loggingIn() {
    return this._loggingIn;
  }
}

export default new AuthStore();
