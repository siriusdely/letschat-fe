
class UnreadsStore {
  constructor() {
    this._unreads = {};
  }

  increment(type, id) {
    const key = `${type}_${id}`;
    let unreads = this._unreads[key] || 0;
    unreads++;
    this._unreads[key] = unreads;
    // console.log('increment ' + key, unreads);
    const event = new Event('UNREADS_CHANGE', {
      detail: {
        unreads,
        time: new Date()
      },
      bubbles: false,
      cancelable: true
    });
    window.dispatchEvent(event);
  }

  decrement(type, id) {
    const key = `${type}_${id}`;
    let unreads = this._unreads[key] || 0;
    unreads--;
    if (unreads < 0) unreads = 0;
    this._unreads[key] = unreads;
    // console.log('decrement ' + key, unreads);
    const event = new Event('UNREADS_CHANGE', {
      detail: {
        unreads,
        time: new Date()
      },
      bubbles: false,
      cancelable: true
    });
    window.dispatchEvent(event);
  }

  unreads(type, id) {
    const key = `${type}_${id}`;
    let unreads = this._unreads[key] || 0;
    // console.log('unreads ' + key, unreads);
    return unreads;
  }
}

export default new UnreadsStore();
