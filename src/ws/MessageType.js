const MessageType = {
  TEXT      : 1,
	SENT      : 2,
	DELIVERED : 3,
	READ      : 4,
};

module.exports = MessageType;
