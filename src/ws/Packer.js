import * as notepack from "msgpack-lite";
import MessageType from './MessageType';

class Packer {

  pack(message, callback) {
    const { type, conversationId, senderId, messageId, content } = message;
    let encoded;

    try {
      if (type === MessageType.TEXT) {
        encoded = notepack.encode([type, conversationId, senderId, content]);
      } else if (type === MessageType.READ ||
                type === MessageType.DELIVERED) {
        encoded = notepack.encode([type, conversationId, senderId, messageId]);
      }
    } catch (e) {
      return callback(e, null);
    }
    return callback(null, encoded);
  }

  unpack(data, callback) {
    let decoded;
    try {
      decoded = notepack.decode(data);
    } catch (err) {
      if (err) { return callback(err, null); }
    }
    // console.log('unpack decoded', decoded);

    const type = decoded[0];
    let message;
    if (type === MessageType.TEXT) {
      message = {
        type,
        conversationId: decoded[1],
        senderId: decoded[2],
        messageId: decoded[3],
        content: decoded[4]
      };
      // console.log('unpack message', message);
    } else if (type === MessageType.SENT ||
               type === MessageType.DELIVERED ||
               type === MessageType.READ) {
      message = {
        type,
        conversationId: decoded[1],
        senderId: decoded[2],
        messageId: decoded[3]
      };
    } else {
      message = {
        type,
        conversationId: decoded[1],
        senderId: decoded[2],
        messageId: decoded[3],
        content: decoded[4]
      };
      return (new Error('Unknown type'), message);
    }

    return callback(null, message);
  }
}

export const packer = new Packer();
