import { packer } from './Packer';
import MessageType from './MessageType';


const _connectingTimeoutDuration = 1*5*1000;

class Connection {
  serverUrl: string;
  userId: string;
  token: string;
  deviceId: string;
  connectionUrl: string;
  connection: WebSocket;
  _onConnect;
  _onDisconnect;
  _onMessage_;
  _reconnect;
  _connectingTimeout;
  _initialTimestamp;

  constructor(serverUrl: string, userId: string, token: string, deviceId: string) {
    this.token = token;
    this.userId = userId;
    this.deviceId = deviceId;
    this.serverUrl = serverUrl;
    this.connectionUrl = serverUrl;
    this._reconnect = true;

    this.onOpen = this._onOpen.bind(this);
    this.onClose = this._onClose.bind(this);
  }

  set onConnect(onConn) {
    this._onConnect = onConn;
  }

  get onConnect() {
    return this._onConnect;
  }

  set onDisconnect(onDisconn) {
    this._onDisconnect = onDisconn;
  }

  get onDisconnect() {
    return this._onDisconnect;
  }

  set onMessage(onMess) {
    this._onMessage_ = onMess;
  }

  get onMessage() {
    return this._onMessage_;
  }

  open() {
    console.log('open: ', this.connectionUrl);
    if (!this._initialTimestamp) { this._initialTimestamp = Date.now(); }
    if (this.connection) { this.close(); }

    this._reconnect = true;

    try {
      this.connection = new WebSocket(this.connectionUrl, [this.token, this.deviceId]);
      if (this.onConnecting) { this.onConnecting(); }
    } catch(e) {
      console.log('catch: ', e);
      this.close();
      return;
    }

    this.connection.binaryType = 'arraybuffer';
    this.connection.onerror = this._onError.bind(this);
    this.connection.onopen = this.onOpen;
    this.connection.onclose = this.onClose;
    this.connection.onmessage = this._onMessage.bind(this);

    if (this._connectingTimeout) {
      clearTimeout(this._connectingTimeout);
      this._connectingTimeout = null;
    }

    this._connectingTimeout = setTimeout(this._connectionCheck, _connectingTimeoutDuration);
  }

  sendText = (conversationId, content) => {
    packer.pack({
      type: MessageType.TEXT,
      conversationId,
      senderId: this.userId,
      content
    }, (err, packed) => {
      if (err) { console.error('sendText pack error', err); }
      // console.log('sendText', packed);
      // console.log('sendText');
      this.connection.send(packed)
    });
  }

  sendRead = (conversationId, messageId) => {
    this._sendRead(conversationId, messageId);
  }

  _sendRead = (conversationId, messageId) => {
    packer.pack({
      type: MessageType.READ,
      conversationId,
      senderId: this.userId,
      messageId
    }, (err, packed) => {
      if (err) { console.error('_sendRead pack error', err); }
      // console.log('_sendRead', packed);
      // console.log('_sendRead');
      this.connection.send(packed);
    });
  }

  _receive = (conversationId, messageId) => {
    packer.pack({
      type: MessageType.DELIVERED,
      conversationId,
      senderId: this.userId,
      messageId
    }, (err, packed) => {
      if (err) { console.error('_receive pack error', err); }
      // console.log('_receive', packed);
      // console.log('_receive');
      this.connection.send(packed);
    });
  }

  _onOpen(e) {
    // console.log('_onOpen: ', e);
    if (this.onConnect) { this.onConnect(); }
  }

  _onMessageSent = (msg) => {
    const { conversationId, senderId, messageId } = msg;
    if (this.onMessageSent) {
      this.onMessageSent({ conversationId, senderId, messageId });
    }
  }

  _onMessageDelivered = (msg) => {
    const { conversationId, senderId, messageId } = msg;
    if (this.onMessageDelivered) {
      this.onMessageDelivered({ conversationId, senderId, messageId });
    }
  }

  _onMessageRead = (msg) => {
    const { conversationId, senderId, messageId } = msg;
    if (this.onMessageRead) {
      this.onMessageRead({ conversationId, senderId, messageId });
    }
  }

  _onReceiveText = (msg) => {
    const { conversationId, senderId, messageId, content } = msg;

    this._receive(conversationId, messageId);
    if (this.onReceiveText) {
      this.onReceiveText({ conversationId, senderId, messageId, content });
    }
  }

  _onMessage(e) {
    // console.log('_onMessage: ', e);
    packer.unpack(new Uint8Array(e.data), (function(err, msg) {
      if (err) { console.error('_onMessage unpack error: ', err); }
      // console.log('_onMessage unpack: ', msg);

      // if (this.onMessage) { this.onMessage(msg); }

      switch(msg.type) {
      case MessageType.TEXT:
        this._onReceiveText(msg);
        break;
      case MessageType.SENT:
        this._onMessageSent(msg);
        break;
      case MessageType.DELIVERED:
        this._onMessageDelivered(msg);
        break;
      case MessageType.READ:
        this._onMessageRead(msg);
        break;
      default:
        console.info('_onMessage unpack UNKNOWN type', msg);
        break;
      }
    }).bind(this));
  }

  _onClose(e) {
    if (e) {
      const { type, reason, code, wasClean, timeStamp } = e;
      console.log(`_onClose => type: ${type} reason: ${reason} code: ${code} wasClean: ${wasClean} timeStamp: ${timeStamp}`, e);
    } else {
      console.log(`_onClose without:`, e);
    }
    if (this.onDisconnect) { this.onDisconnect(); }
    if (this._reconnect) { this.open(); }
  }

  _onError(e) {
    const { type, timeStamp } = e;
    console.log(`_onError => type: ${type} timeStamp: ${timeStamp}`, e);
  }

  _connectionCheck = () => {
    if (!this.connection) { return; }
    const readyState = this.connection.readyState;
    console.log('_connectionCheck readyState', readyState);
    if (readyState !== 1) {
      this.open();
    } else if (this._connectingTimeout) {
      clearTimeout(this._connectingTimeout);
      this._connectingTimeout = null;
    }
  }

  close() {
    this._reconnect = false;
    if (this.connection) { this.connection.close(); }
    this.connection = null;
  }
}

export default Connection;
