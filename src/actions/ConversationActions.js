import {
  LOAD_CONVERSATIONS, SELECT_CONVERSATION
} from '../constants/ConversationConstants';
import AppDispatcher from '../stores/AppDispatcher';

export default {
  loadConversations: () => {
    AppDispatcher.dispatch({ type: LOAD_CONVERSATIONS });
  },
  selectConversation: function(conversation) {
    AppDispatcher.dispatch({
      type: SELECT_CONVERSATION,
      conversation
    });
  }
};
