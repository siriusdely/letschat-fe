import {
  RECEIVE_TEXT,
  READ_MESSAGE,
  SEND_TEXT,
  MESSAGE_SENT,
  MESSAGE_DELIVERED,
  MESSAGE_READ
} from '../constants/MessageConstants';
import AppDispatcher from '../stores/AppDispatcher';

export default {
  receiveText: function(conversationId, senderId, messageId, content) {
    AppDispatcher.dispatch({
      type: RECEIVE_TEXT,
      conversationId,
      senderId,
      messageId,
      content
    });
  },
  readMessage: function(conversationId, messageId) {
    AppDispatcher.dispatch({
      type: READ_MESSAGE,
      conversationId,
      messageId
    });
  },
  sendText: function(conversationId, content) {
    AppDispatcher.dispatch({
      type: SEND_TEXT,
      conversationId,
      content
    });
  },
  messageSent: function(conversationId, senderId, messageId) {
    AppDispatcher.dispatch({
      type: MESSAGE_SENT,
      conversationId,
      senderId,
      messageId
    });
  },
  messageDelivered: function(conversationId, senderId, messageId) {
    AppDispatcher.dispatch({
      type: MESSAGE_DELIVERED,
      conversationId,
      senderId,
      messageId
    });
  },
  messageRead: function(conversationId, senderId, messageId) {
    AppDispatcher.dispatch({
      type: MESSAGE_READ,
      conversationId,
      senderId,
      messageId
    });
  },
};
