import {
  LOAD_CONTACTS, LOAD_USERS, LOAD_GROUPS
} from '../constants/ContactConstants';
import AppDispatcher from '../stores/AppDispatcher';

export default {
  loadContacts: () => {
    AppDispatcher.dispatch({ type: LOAD_CONTACTS });
  },
  loadUsers: () => {
    AppDispatcher.dispatch({ type: LOAD_USERS });
  }
  , loadGroups: () => {
    AppDispatcher.dispatch({ type: LOAD_GROUPS });
  }
};
