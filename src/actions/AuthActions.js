import { START_LOGIN, LOGOUT } from '../constants/AuthConstants';
import AppDispatcher from '../stores/AppDispatcher';

export default {
  startLogin: function(username, password) {
    AppDispatcher.dispatch({
      type: START_LOGIN,
      username,
      password,
    });
  },
  logout: function() {
    AppDispatcher.dispatch({ type: LOGOUT });
  }
};
