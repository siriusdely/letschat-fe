import React, { Component } from 'react';

import MessageRow from './MessageRow';
import MessagesStore from '../stores/MessagesStore';
import MessageActions from '../actions/MessageActions';

class MessagesList extends Component {

  constructor(props) {
    super(props);
    this.state = this._state(props);
  }

  _state(props) {
    const { conversation } = props;

    let messages = MessagesStore.messages(conversation.id);

    return {
      messages
    };
  }

  _handleStoreChange() {
    this.setState(this._state(this.props));
  }

  _handlePlay(messageId, channelType, channelId) {
    MessageActions.play(messageId, channelType, channelId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.conversation !== this.props.conversation) {
      this.setState(this._state(nextProps));
    }
  }

  componentDidMount() {
    this.handleStoreChange = this._handleStoreChange.bind(this);
    MessagesStore.addChangeListener(this.handleStoreChange);
  }

  componentWillUnmount() {
    MessagesStore.removeChangeListener(this.handleStoreChange)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.conversation !== this.props.conversation ||
        prevState.messages.length !== this.state.messages.length) {
      this._scroller.scrollTop = this._scroller.scrollHeight - 0;
    }
  }

  _handleScrollReference(node) {
    if (node) {
      this._scroller = node;
    }
  }

  render() {
    const { messages } = this.state;
    console.log('messages', messages);

    return (
      <div id='messages-container'>
        <div id='messages-panel' ref={ this._handleScrollReference.bind(this) }>
          <ul id='scroller' className='chat-box group'>
            {
              messages.map(m => {
                if (m.status === 'RECEIVED') {
                  setTimeout(function() {
                    MessageActions.readMessage(m.conversationId, m.id);
                  });
                }
                return (<MessageRow key={ m.id } content={ m.content } startedAt={ m.startedAt }
                                    status={ m.status } sender={ m.sender } />);
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default MessagesList;
