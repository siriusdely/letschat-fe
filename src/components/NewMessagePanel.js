import React, { Component } from 'react';

class NewMessagePanel extends Component {

  state = {
    content: ''
  };

  _handleChange(e) {
    this.setState({ content: e.target.value });
  }

  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      if (!e.shiftKey) {
        e.preventDefault();
        e.stopPropagation();
        this._handleSend.bind(this)(e);
      }
    }
  }

  _handleSend(e) {
    const { content } = this.state;
    if (!content) return;
    this.props.onSend(content);
    this.setState({ content: '' });
  }

  render() {
    const { content } = this.state;

    return (
      <div id='send-message-panel'>
        <textarea
          placeholder='New Message'
          value={ content }
          onChange={ this._handleChange.bind(this) }
          onKeyPress={ this._handleKeyPress.bind(this) }
          ref={ (ref) => { this.textInput = ref; } }
        />
        <a onClick={ this._handleSend.bind(this) } title='Send'>
          <i className='material-icons'>send</i>
        </a>
      </div>
    );
  }

}

export default NewMessagePanel;
