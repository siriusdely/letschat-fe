import React, { Component } from 'react';

import WelcomeView from './WelcomeView';

import Avatar from './Avatar';
import MessagesList from './MessagesList';
import NewMessagePanel from './NewMessagePanel';

import ConversationsStore from '../stores/ConversationsStore';
import MessageActions from '../actions/MessageActions';

class MessagesView extends Component {

  state = {
    conversation: ConversationsStore.conversation
  };

  _handleConversationChange() {
    this.setState({
      conversation: ConversationsStore.conversation
    });
  }

  _handleSendText(text) {
    // console.log('_handleSendText ' + text, this.state.conversation);
    const { conversation } = this.state;
    MessageActions.sendText(conversation.id, text);
  }

  constructor(props) {
    super();
    this.handleConversationChange = this._handleConversationChange.bind(this);
  }

  componentDidMount() {
    ConversationsStore.addChangeListener(this.handleConversationChange);
  }

  componentWillUnmount() {
    ConversationsStore.removeChangeListener(this.handleConversationChange);
  }

  render() {
    const { conversation } = this.state;

    let title = 'LetsChat';
    let component;
    if (conversation) {
      title = conversation.title;
      component = (
        <div id='topic-view'>
          <div id='topic-caption-panel' className='caption-panel'>
            <div className='avatar-box'>
              <Avatar title={ title } />
            </div>
            <div id='topic-title-group'>
              <div id='topic-title' className='panel-title'>{ title }</div>
            </div>
          </div>
          <MessagesList conversation={ conversation } />
          <NewMessagePanel
            onSend={ this._handleSendText.bind(this) } />
        </div>
      );
    } else {
      component = <WelcomeView />;
    }

    return component;
  }
}

export default MessagesView
