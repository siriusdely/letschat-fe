import React, { Component } from 'react';

import MessagesView from './MessagesView';
import SidePanelView from './SidePanelView';

class App extends Component {

  render() {
    return (
      <div id="app-container">
        <SidePanelView />
        <MessagesView />
      </div>
    );
  }
}

export default App;
