import React, { Component } from 'react';

import Avatar from './Avatar';

class ContactRow extends Component {

  handleClick(e) {
    e.preventDefault();
    e.stopPropagation();
    this.props.onSelectContact(this.props.contact);
  }

  render() {
    return (
      <li onClick={ this.handleClick.bind(this) }>
        <div className="avatar-box">
          <Avatar avatarUrl={ this.props.contact.avatarUrl } title={ this.props.fullname } />
        </div>
        <div className="text-box">
          <div>
            <span className="contact-title">{ this.props.fullname }</span>
          </div>
        </div>
      </li>
    );
  }

}

export default ContactRow;
