import React, { Component } from 'react';

import ContactRow from './ContactRow';

import ContactActions from '../actions/ContactActions';
import ContactsStore from '../stores/ContactsStore';

import ConversationActions from '../actions/ConversationActions';

class ContactsList extends Component {
  _state(keyword) {
    let users = ContactsStore.users || [];
    let groups = ContactsStore.groups || [];

    if (keyword) {
      users = users.filter(c => c.fullname.toLowerCase().indexOf(keyword.toLowerCase()) !== -1);
      groups = groups.filter(c => c.fullname.toLowerCase().indexOf(keyword.toLowerCase()) !== -1);
    }

    return {
      users,
      groups
    }
  }

  constructor(props) {
    super(props);
    this.state = this._state(props.keyword);
    this.handleStoreChange = this._handleStoreChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps: ', nextProps);
    const { contactType, keyword } = nextProps;
    if (this.props.contactType !== contactType) {
      if (contactType === 'GROUP') {
        ContactActions.loadGroups();
      } else {
        ContactActions.loadUsers();
      }
    }
    if (this.props.keyword !== keyword) {
      this.setState(this._state(keyword));
    }
  }

  _handleStoreChange() {
    const { keyword } = this.props;
    this.setState(this._state(keyword));
  }

  _handleSelectContact(contact) {
    ConversationActions.focusTopic(contact);
    this.props.onSelectContact();
  }

  componentDidMount() {
    ContactsStore.addChangeListener(this.handleStoreChange);
    ContactActions.loadUsers();
  }

  componentWillUnmount() {
    ContactsStore.removeChangeListener(this.handleStoreChange);
  }

  render() {
    const { users, groups } = this.state;
    const { contactType } = this.props;
    const contacts = (contactType === 'GROUP' ? groups : users);

    return (
      <div className="scrollable-panel">
        <ul className="contact-box">
          {
            contacts && contacts.map(c =>
              <ContactRow key={ c.id } fullname={ c.fullname } contact={ c }
                                 onSelectContact={ this._handleSelectContact.bind(this) } />)
          }
        </ul>
      </div>
    );
  }
}

export default ContactsList;
