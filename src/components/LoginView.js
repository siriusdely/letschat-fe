import React, { Component } from 'react';

import AuthActions from '../actions/AuthActions';
import AuthStore from '../stores/AuthStore';

class LoginView extends Component {
  state = {
    loggingIn: false,
    username: AuthStore.username || '',
    password: AuthStore.password || ''
  };

  componentDidMount() {
    this.handleAuthStoreChange = this._handleAuthStoreChange.bind(this);
    AuthStore.addChangeListener(this.handleAuthStoreChange);
  }

  componentWillUnmount() {
    AuthStore.removeChangeListener(this.handleAuthStoreChange);
  }

  _handleAuthStoreChange() {
    this.setState({
      loggingIn: !this.state.loggingIn,
      username: AuthStore.username || '',
      password: AuthStore.password || ''
    });
  }

  _handleChange(e) {
    const { id, value } = e.target;
    if (id === 'username') {
      this.setState({ username: value });
    } else if (id === 'password') {
      this.setState({ password: value });
    }
  }

  _handleSubmit(e) {
    e.preventDefault();
    const { username, password } = this.state;
    AuthActions.startLogin(username, password);
    this.setState({ loggingIn: true });
  }

  render() {
    const { username, password, loggingIn } = this.state;
    const loginEnabled = (!!username && !!password) && !loggingIn;

    return (
      <form id="login-form" onSubmit={ this._handleSubmit.bind(this) }>
        <input
          type="text"
          id="username"
          placeholder="Login"
          value={ username }
          onChange={ this._handleChange.bind(this) }
          required autoFocus
        />
        <input
          type="password"
          id="password"
          placeholder="Password"
          value={ password }
          onChange={ this._handleChange.bind(this) }
          required
        />
        <div className="dialog-buttons">
          {
            loginEnabled ? (
              <button className="blue" type="submit">
                Sign In
              </button>) : (
                <button className="blue" type="submit" disabled>
                  Sign In
                </button>
              )
          }
        </div>
      </form>
    );
  }
}

export default LoginView;
