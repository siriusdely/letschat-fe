import React, { Component } from 'react';

class SearchContactsPanel extends Component {

  state = { keyword: '' };

  _handleChange(e) {
    const keyword = e.target.value;
    this.setState({ keyword });
    this.props.onKeywordChange(keyword);
  }

  _handleKeyDown(e) {
  }

  _handleClear(e) {
    this.setState({ keyword: '' });
    this.props.onKeywordChange('');
  }

  render() {
    const { keyword } = this.state;

    return (
      <div className="panel-form">
        <div className="panel-form-row">
          <i className="material-icons search">search</i>
          <input
            className="search"
            type="text"
            placeholder="search contacts"
            value={ keyword }
            onChange={ this._handleChange.bind(this) }
            onKeyDown={ this._handleKeyDown.bind(this) }
            required autoFocus
          />
          <a onClick={ this._handleClear.bind(this) }>
            <i className="material-icons">close</i>
          </a>
        </div>
      </div>
    );
  }
}

export default SearchContactsPanel;
