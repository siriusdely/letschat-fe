import React, { Component } from 'react';

import ContactsList from './ContactsList';
import SearchContactsPanel from './SearchContactsPanel';

class SearchContactsView extends Component {

  state = { keyword: '' }

  _handleKeywordChange(keyword) {
    this.setState({ keyword });
  }

  render() {
    const { keyword } = this.state;

    return (
      <div className="flex-column">
        <div className="flex-column">
          <SearchContactsPanel onKeywordChange={ this._handleKeywordChange.bind(this) } />
          <ContactsList onSelectContact={ this.props.onClose } keyword={ keyword } />
        </div>
      </div>
    );
  }
}

export default SearchContactsView;
