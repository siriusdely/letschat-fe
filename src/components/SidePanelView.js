import React, { Component } from 'react';

import ConversationsList from './ConversationsList';
import LoginView from './LoginView';
import SearchContactsView from './SearchContactsView';
import SideNavBar from './SideNavBar';
import InfoSubBar from './InfoSubBar';

import AuthActions from '../actions/AuthActions';
import AuthStore from '../stores/AuthStore';
import {
  CONNECTION_CHANGE,
  CONNECTED, CONNECTING, DISCONNECTED
} from '../constants/ConnectionConstants';

import ContactActions from '../actions/ContactActions';
import ConversationActions from '../actions/ConversationActions';

class SidePanelView extends Component {
  constructor() {
    super();
    this.state = this._state();
  }

  _state() {
    let sideNavTitle = '';
    let sideView = '';
    let infoShown = false;
    let infoMessage = '';
    let infoLevel = 'info';

    const { accessToken, user, error } = AuthStore;
    if (!accessToken) {
      sideNavTitle = 'Sign In';
      sideView = 'login';
      if (error) {
        infoShown = true;
        infoMessage = 'check username and/or password';
        infoLevel = 'error';
        setTimeout(() => {
          this.setState({ infoShown: false });
        }, 5000);
      }
    } else {
      sideNavTitle = user.email;
      sideView = 'conversations';
    }

    return {
      sideNavTitle, sideView,
      infoShown, infoLevel, infoMessage
    };
  }

  _handleAuthStoreChange() {
    this.setState(this._state());
    if (AuthStore.accessToken) {
      setTimeout(function() {
        ConversationActions.loadConversations();
        ContactActions.loadContacts();
      });
    }
  }

  _handleConnectionChange(e) {
    // console.log('_handleConnectionChange: ', e.detail);
    const { status: connection } = e.detail;
    let infoShown = false;
    let infoMessage = '';
    let infoLevel = 'info';

    if (connection === DISCONNECTED) {
      infoShown = true;
      infoMessage = 'Disconnected';
      infoLevel = 'error';
    } else if (connection === CONNECTING) {
      infoShown = true;
      infoLevel = 'warning';
      infoMessage = 'Connecting..'
    } else if (connection === CONNECTED) {
      infoShown = true;
      infoMessage = 'Connected';
      infoLevel = 'info';
      setTimeout(() => {
        this.setState({ infoShown: false });
      }, 5000);
    } else {
      infoShown = false;
    }

    this.setState({ infoShown, infoLevel, infoMessage });
  }

  componentDidMount() {
    this.handleAuthStoreChange = this._handleAuthStoreChange.bind(this);
    this.handleConnectionChange = this._handleConnectionChange.bind(this);
    AuthStore.addChangeListener(this.handleAuthStoreChange);
    window.addEventListener(CONNECTION_CHANGE, this.handleConnectionChange, false);
    if (AuthStore.accessToken) {
      ConversationActions.loadConversations();
      ContactActions.loadContacts();
    }
    document.onkeypress = this._handleKeyPress.bind(this);
  }

  componentWillUnmount() {
    AuthStore.removeChangeListener(this.handleAuthStoreChange);
    window.removeEventListener(CONNECTION_CHANGE, this.handleConnectionChange, false);
    document.onkeydown = null;
  }

  _handleKeyPress(e) {
    // console.log('_handleKeyPress: ' + e.keyCode, e);
    // if (e.ctrlKey && e.keyCode === 192) {
    if (e.ctrlKey && e.keyCode === 96) {
      if (this.state.sideView !== 'contacts') {
        this.handleNavBarMenuClick('contacts');
      } else {
        this.handleNavBarMenuClose();
      }
    } else if (e.keyCode === 27 && this.state.sideView === 'contacts') {
      this.handleNavBarMenuClose();
    }
  }

  handleNavBarMenuClick = (menuTitle) => {
    if (menuTitle === 'signout') {
      if (window.confirm('LetsChat will logout:')) {
        // console.log('LOGOUT');
        AuthActions.logout();
      }
      return;
    }

    let sideNavTitle = 'Sirius';
    if (menuTitle === 'contacts') {
      sideNavTitle = 'Start New Chat';
    }
    this.setState({
      sideNavTitle,
      sideView: menuTitle
    });
  }

  handleNavBarMenuClose = () => {
    const user = AuthStore.user;
    this.setState({
      sideNavTitle: user.email,
      sideView: 'conversations'
    });
  }

  render() {
    const {
      sideNavTitle, sideView,
      infoShown, infoMessage, infoLevel
    } = this.state;

    let onMenuClose = null;
    let sideViewComponent = null;
    let avatar = false;
    if (sideView === 'conversations') {
      sideViewComponent = <ConversationsList />;
      avatar = true;
    } else if (sideView === 'contacts') {
      onMenuClose = this.handleNavBarMenuClose;
      sideViewComponent = <SearchContactsView onClose={ this.handleNavBarMenuClose } />;
    } else {
      sideViewComponent = <LoginView />;
    }

    const user = AuthStore.user;

    return (
      <div id="sidepanel">
        <SideNavBar
          onMenuClick={ this.handleNavBarMenuClick }
          onMenuClose={ onMenuClose }
          avatar={ avatar }
          avatarUrl={ user && user.avatarUrl }
          title={ sideNavTitle }
        />
        { infoShown ? <InfoSubBar level={ infoLevel } info={ infoMessage} /> : null }
        { sideViewComponent }
      </div>
    );
  }
}

export default SidePanelView;
