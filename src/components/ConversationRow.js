import React, { Component } from 'react';

import Avatar from './Avatar';

class ConversationRow extends Component {

  handleClick(e) {
    e.preventDefault();
    e.stopPropagation();
    this.props.onSelectConversation(this.props.conversation);
  }

  render() {
    const { title, selected, unreads } = this.props;
    // console.log('unreads: ', unreads);

    let unread = null;
    if (unreads > 0) {
      const count = unreads > 9 ? '9+' : unreads;
      unread = <span className="unread">{ count }</span>;
    }

    return (
      <li className={ selected ? 'selected' : '' }
          onClick={ this.handleClick.bind(this) }>
        <div className="avatar-box">
          <Avatar title={ title } />
        </div>
        <div className="text-box">
          <div>
            <span className="contact-title">{ title }</span>
            { unread }
          </div>
        </div>
      </li>
    );
  }

}

export default ConversationRow;
