import React, { Component } from 'react';

import ConversationRow from './ConversationRow';

import ConversationActions from '../actions/ConversationActions';
import ConversationsStore from '../stores/ConversationsStore';
import UnreadsStore from '../stores/UnreadsStore';

class ConversationsList extends Component {

  _state() {
    return {
      conversations: ConversationsStore.conversations,
      conversation: ConversationsStore.conversation
    };
  }

  constructor() {
    super();
    this.state = this._state();
    this.handleStoreChange = this._handleStoreChange.bind(this);
    this.handleUnreadsChange = this._handleUnreadsChange.bind(this);
  }

  componentDidMount() {
    ConversationsStore.addChangeListener(this.handleStoreChange);
    window.addEventListener('UNREADS_CHANGE', this.handleUnreadsChange, false);
  }

  componentWillUnmount() {
    ConversationsStore.removeChangeListener(this.handleStoreChange);
    window.removeEventListener('UNREADS_CHANGE', this.handleUnreadsChange, false);
  }

  _handleStoreChange() {
    this.setState(this._state());
  }

  _handleUnreadsChange(e) {
    // console.log('_handleUnreadsChange');
    this.forceUpdate();
  }

  handleSelectConversation(conversation) {
    ConversationActions.selectConversation(conversation);
  }

  render() {
    const { conversations, conversation } = this.state;
    // console.log('conversations', conversations);

    return (
      <div className="scrollable-panel">
        { conversations && conversations.length ?
          <ul className="contact-box">
            {
              conversations.map(c =>
                <ConversationRow
                  key={ c.id } title={ c.title } conversation={ c } selected={ c.id === conversation.id }
                  unreads={ UnreadsStore.unreads((c.type === 'user' ? 1 : 0), c.id) }
                  onSelectConversation={ this.handleSelectConversation } />)
            }
          </ul> :
          <div className='center-medium-text'>
            <span>You have no chats<br />¯\_(ツ)_/¯</span>
          </div>
        }
      </div>
    );
  }
}

export default ConversationsList;
