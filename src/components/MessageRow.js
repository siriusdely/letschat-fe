import React, { Component } from 'react';

import Avatar from './Avatar';

import { shortDateFormat } from '../utils';

class MessageRow extends Component {
  render() {
    const { content, status, sender, startedAt } = this.props;

    let className = 'single';
    if (status === 'RECEIVED' || status === 'RECEIVED_READ') {
      className += ' left';
    } else if (status === 'UNSENT') {
      className += ' unsent';
    } else {
      className += ' right';
    }

    let marker = null;
    if (status === 'UNSENT') {
      marker = <i className='material-icons small'>access_time</i>;
    } else if (status === 'READ') {
      marker = <i className='material-icons small'>done_all</i>;
    } else if (status === 'DELIVERED') {
      marker = <i className='material-icons small'>done</i>;
    }

    let avatar = null;
    let author = null;
    if (sender) {
      const { fullname, avatarUrl } = sender;
      avatar = <Avatar title={ fullname } avatarUrl={ avatarUrl } />;
      author = <div className="author">{ fullname }</div>
    }

    const timestamp = shortDateFormat(startedAt);

    return (
      <li className={ className }>
        <div className='avatar-box'>
          { avatar }
        </div>
        <div>
          <div className='bubble tip'>
            <div className='message-content'>
              { content }
              <span className='timestamp'>
                { timestamp }{ '\u00a0' }{ marker }
              </span>
            </div>
          </div>
          { author }
        </div>
      </li>
    );
  }
}

export default MessageRow;
