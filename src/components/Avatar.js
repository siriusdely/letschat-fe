import React, { PureComponent } from 'react';
import { stringHash } from '../utils';

export default class Avatar extends PureComponent {
  render() {
    let { avatarUrl, title } = this.props;

    let avatar;
    if (avatarUrl) {
      avatar = <img className='avatar' alt={ 'title' } src={ avatarUrl } />;
    } else {
      title = title && title.trim();
      const letter = title && title.charAt(0);

      if (letter) {
        const color = 'lettertile dark-color' + (Math.abs(stringHash(title)) % 16);
        avatar = <div className={ color}><div>{ letter }</div></div>;
      } else {
        avatar = <i className='material-icons'>person</i>;
      }
    }

    return avatar;
  }
}
