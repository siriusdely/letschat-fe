import React, { Component } from 'react';

import Avatar from './Avatar';
import MenuContacts from './MenuContacts';

class SideNavBar extends Component {
  render() {
    const { avatar, avatarUrl, title,
            onMenuClick, onMenuClose } = this.props;

    let ava = null;
    if (avatar) {
      ava = (<div id='self-avatar' className='avatar-box'>
        <Avatar avatarUrl={ avatarUrl } title={ title } /></div>);
    }

    let menu = null;
    if (title !== 'Sign In') {
      menu = (<MenuContacts onMenuClick={ onMenuClick }
        onMenuClose={ onMenuClose } />);
    }

    return (
      <div id='side-caption-panel' className='caption-panel'>
        { ava }
        <div id='sidepanel-title' className='panel-title'>{ title }</div>
        { menu }
      </div>
    );
  }
}

export default SideNavBar;
