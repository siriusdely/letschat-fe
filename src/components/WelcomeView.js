import React from 'react';

export default function WelcomeView() {
  return (
    <div id="dummy-view">
      <div>
        <a href="#" rel="noopener noreferrer">
          <h2>LetsChat</h2>
        </a>
      </div>
    </div>
  );
}
