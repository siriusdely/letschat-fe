import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MenuContacts extends Component {
  render() {
    const { onMenuClick, onMenuClose } = this.props;

    return (
      <div>
        { onMenuClose &&
          <a onClick={ () => onMenuClose() }>
            <i className="material-icons">close</i></a>
        }
        { !onMenuClose &&
          <a onClick={ () => onMenuClick("contacts") }>
            <i className="material-icons">group</i></a>
        }
        { !onMenuClose &&
          <a onClick={ () => onMenuClick('signout') }>
            <i className="material-icons">exit_to_app</i></a>
        }
      </div>
    );
  }
}

MenuContacts.propTypes = {
  onMenuClick: PropTypes.func.isRequired,
  onMenuClose: PropTypes.func
};

export default MenuContacts;
