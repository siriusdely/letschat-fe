import React, { Component } from 'react';

export default class InfoSubBar extends Component {
  render() {
    const { level: icon, info: text } = this.props;
    const className = 'alert-box ' + (icon === 'info' ? 'success' : icon);
    return (
      <div className={ className }>
        <div className="icon"><i className="material-icons">{ icon }</i></div>
        { text }
      </div>
    );
  }
}
