const MessageStatus = {
  RECEIVED: 'RECEIVED',
  RECEIVED_READ: 'RECEIVED_READ',
  UNSENT: 'UNSENT',
  SENT: 'SENT',
  DELIVERED: 'DELIVERED',
  READ: 'READ',
  FAILED: 'FAILED'
}

module.exports = MessageStatus;
