const ConnectionConstants = {
  CONNECTION_CHANGE: 'CONNECTION_CHANGE',
  CONNECTED: 'CONNECTED',
  CONNECTING: 'CONNECTING',
  DISCONNECTED: 'DISCONNECTED',
  SERVER_URL: process.env.REACT_APP_SERVICE_HOST || 'ws://localhost:3002'
};

module.exports = ConnectionConstants;
