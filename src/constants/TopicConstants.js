const TopicConstants = {
  ADD_TOPIC: 'ADD_TOPIC',
  SELECT_TOPIC: 'SELECT_TOPIC',
  FOCUS_TOPIC: 'FOCUS_TOPIC'
};

module.exports = TopicConstants;
