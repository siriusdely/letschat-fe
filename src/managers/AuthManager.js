import LetsChatAPI from './LetsChatAPI';

import {
  LOGIN_SUCCEED, LOGIN_FAILED
} from '../constants/AuthConstants';
import AppDispatcher from '../stores/AppDispatcher';

const AuthManager = {
  login(email: string, password: string) {
    LetsChatAPI
      .post('/login', { email, password })
      .then(result => {
        // console.log('AuthManager:login: ', result);
        const { status: code, statusText: message, data } = result;
        if (code !== 200) {
          const error = { code, message };
          // console.error('AuthManager:login:error: ', error, data);
          AppDispatcher.dispatch({ type: LOGIN_FAILED, error });
        } else {
          const { token, user: { id, email, conversation_ids: conversationIds } } = data;
          const user = { id, email, conversationIds};
          // console.log('AuthManager:login:user: ', user);
          AppDispatcher.dispatch({
            type: LOGIN_SUCCEED,
            token,
            user
          });
        }
      });
  }
};

export default AuthManager;
