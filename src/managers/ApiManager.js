import LetsChatAPI from './LetsChatAPI';

import {
  LOAD_CONTACTS_SUCCEED, LOAD_CONTACTS_FAILED,
} from '../constants/ContactConstants';
import {
  LOAD_CONVERSATIONS_SUCCEED, LOAD_CONVERSATIONS_FAILED
} from '../constants/ConversationConstants';
import { UNAUTHORIZED } from '../constants/AuthConstants';
import AppDispatcher from '../stores/AppDispatcher';

class ApiManager {
  _accessToken;

  constructor() {
    this._accessToken = null;
  }

  set accessToken(accessToken): void {
    this._accessToken = accessToken;
    LetsChatAPI.token(accessToken);
  }

  _handleError(error, type) {
    console.log(type, error);
    if (error.code === 401) {
      this._handleUnauthorized();
    } else {
      AppDispatcher.dispatch({ type, error });
    }
  }

  _handleUnauthorized() {
    console.log('_handleUnauthorized');
    AppDispatcher.dispatch({ type: UNAUTHORIZED });
  }

  getContacts() {
    LetsChatAPI
      .get('/contacts')
      .then(result => {
        // console.log("getContacts result", result);
        const { status: code, statusText: message, data } = result;
        if (code !== 200) {
          const error = { code, message };
          // console.error('getContacts', error);
          this._handleError(error, LOAD_CONTACTS_FAILED);
        } else {
          const contacts = data.contacts;
          AppDispatcher.dispatch({
            type: LOAD_CONTACTS_SUCCEED,
            contacts
          });
        }
      });
  }

  getConversations() {
    LetsChatAPI
      .get('/conversations')
      .then(result => {
        // console.log("getConversations result", result);
        const { status: code, statusText: message, data } = result;
        if (code !== 200) {
          const error = { code, message };
          // console.error('getConversations', error);
          this._handleError(error, LOAD_CONVERSATIONS_FAILED);
        } else {
          const conversations = data.conversations;
          // console.log("getConversations conversations", conversations);
          AppDispatcher.dispatch({
            type: LOAD_CONVERSATIONS_SUCCEED,
            conversations
          });
        }
      });
  }
}

export default new ApiManager();
