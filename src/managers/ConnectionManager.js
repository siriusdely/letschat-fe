import {
  CONNECTION_CHANGE, CONNECTED, CONNECTING, DISCONNECTED,
  SERVER_URL } from '../constants/ConnectionConstants';

import MessageActions from '../actions/MessageActions';
import Connection from '../ws/Connection';

class ConnectionManager {
  _token: string;
  user;
  connection;

  connect() {
    if (!this.deviceId) {
      this.deviceId = `${this.user.id}-${Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)}-${Date.now()}`;
      console.log('deviceId: ', this.deviceId);
    }
    const connection = new Connection(SERVER_URL, this.user.id, this.token, this.deviceId);
    connection.onConnecting = this._onConnecting.bind(this);
    connection.onConnect = this._onConnect.bind(this);
    connection.onDisconnect = this._onDisconnect.bind(this);
    // connection.onMessage = this._onMessage.bind(this);
    connection.onMessageSent = this._onMessageSent.bind(this);
    connection.onMessageDelivered = this._onMessageDelivered.bind(this);
    connection.onMessageRead = this._onMessageRead.bind(this);
    connection.onReceiveText = this._onReceiveText.bind(this);
    connection.open();
    this.connection = connection;
  }

  disconnect() {
    this.connection.close();
  }

  sendText(conversationId, content) {
    this.connection.sendText(conversationId, content);
  }

  sendRead(conversationId, messageId) {
    this.connection.sendRead(conversationId, messageId);
  }

  _onConnect() {
    console.log('ConnectionManager _onConnect');
    const event = new CustomEvent(CONNECTION_CHANGE, {
      detail: {
        status: CONNECTED,
        time: new Date()
      },
      bubbles: false,
      cancelable: true
    });
    window.dispatchEvent(event);
  }

  _onConnecting() {
    console.log('ConnectionManager _onConnecting');
    const event = new CustomEvent(CONNECTION_CHANGE, {
      detail: {
        status: CONNECTING,
        time: new Date()
      },
      bubbles: false,
      cancelable: true
    });
    window.dispatchEvent(event);
  }

  _onDisconnect() {
    console.log('ConnectionManager _onDisconnect');
    const event = new CustomEvent(CONNECTION_CHANGE, {
      detail: {
        status: DISCONNECTED,
        time: new Date()
      },
      bubbles: false,
      cancelable: true
    });
    window.dispatchEvent(event);
  }

  _onMessage(msg) {
    // console.log('ConnectionManager _onMessage', msg);
  }

  _onMessageSent(msg) {
    // console.log('ConnectionManager _onMessageSent', msg);
    MessageActions.messageSent(msg.conversationId, msg.senderId, msg.messageId);
  }

  _onMessageDelivered(msg) {
    // console.log('ConnectionManager _onMessageDelivered', msg);
    MessageActions.messageDelivered(msg.conversationId, msg.senderId, msg.messageId);
  }

  _onMessageRead(msg) {
    // console.log('ConnectionManager _onMessageRead', msg);
    MessageActions.messageRead(msg.conversationId, msg.senderId, msg.messageId);
  }

  _onReceiveText(msg) {
    // console.log('ConnectionManager _onReceiveText', msg);
    MessageActions.receiveText(msg.conversationId, msg.senderId, msg.messageId, msg.content);
  }

  set token(token: string) {
    console.log('token: ', token);
    this._token = token;
  }

  get token() {
    return this._token;
  }
}

export default new ConnectionManager();
