// @flow

import axios from 'axios';

// The port is hard coded in the server too. If you change it make sure to
// update it there as well.

const HOST = process.env.REACT_APP_API_HOST || 'http://localhost:3001';
const PATH = '/api/v1';

// Using some flow trickery we can strongly type our requests! We don't verify
// this at runtime though, so it's not actually sound. But we're all good if
// we trust the API implementation :)
declare class LetsChatAPI {

  static post(uri: '/login',
              data: {email: string, password: string}): Promise<Object>;

  static get(uri: '/users'): Promise<Object>;

  static token(tkn: string): void;

}

let _tkn: ?string = null;

// $FlowExpectedError: Intentional rebinding of variable.
const LetsChatAPI = {  // eslint-disable-line
  get(uri, data) {
    return promiseXHR('get', uri, data);
  },

  post(uri, data) {
    return promiseXHR('post', uri, data);
  },

  token(tkn: string) {
    _tkn = tkn;
  }
};

/**
 * This is a simple wrapper around XHR that let's us use promises. Not very
 * advanced but works with our server's API.
 */
function promiseXHR(method: 'get'|'post', uri, data) {
  let url = HOST;
  switch(uri) {
  case '/login':
    url += PATH + '/auth/login';
    break;
  default:
    url += PATH + uri;
    break;
  }

  let headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };

  if (_tkn) {
    headers = {
      ...headers,
      Authorization: 'Bearer ' + _tkn,
    }
  }

  let options = {
    url,
    headers,
    method,
    data,
    // timeout: 5000, // default is 0 (no timeout)
    validateStatus: function (status) {
      return status < 501;
    },
  };

  try {
    return axios({ ...options });
  } catch (error) {
    return error;
  }
}

export default LetsChatAPI;
